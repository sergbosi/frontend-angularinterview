**Prerequisites **

1. NodeJs [link](https://nodejs.org/en/) version 8.12.0 or above
2. lastest version of npm version 6.4.1 or above
3. Angular-cli [link](https://cli.angular.io/) version 6.2.1 or above
4. Git


#Direct test
1. go to /dist folder and open a terminal
2. install a static http server

   - npm install http-server -g
   - to use the server simply run : http-server
   - enter one of the ips shown by the server
 

# build the project First you need to checkout the project

1. git clone https://sergbosi@bitbucket.org/sergbosi/frontend-angularinterview.git
2. the go the the directory where you clonned the repository and open a terminal and run the following commands:
3. npm install
4. ng serve


if the docker is running or the [backend](https://bitbucket.org/sergbosi/backend-javainterview/src/master/) is running on localhost:8080 it will connect.
then go to [localhost:4200](http://localhost:4200/)

And you can see the app running
