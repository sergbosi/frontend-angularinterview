import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent} from './footer/footer.component';
 
import { EventsComponent } from './events/events.component';
import { EventService } from './events/event.service';
import { RouterModule, Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './events/form.component';
import { FormsModule } from '@angular/forms'
 
const routes: Routes = [
  {path: '', redirectTo: '/Events', pathMatch: 'full'},
  {path: 'Events', component: EventsComponent},
  {path: 'Events/form', component: FormComponent},
  {path: 'Events/form/:id', component: FormComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    EventsComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
