import { Component, OnInit } from '@angular/core';
import { Venue } from './venue';
import { Event } from './event';
import { EventService } from './event.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-Events',
  templateUrl: './events.component.html'
})
export class EventsComponent implements OnInit {

  Events: Event[];

  constructor(private EventService: EventService) { }

  ngOnInit() {
    this.EventService.getEvents().subscribe(
      Events => this.Events = Events
    );
  }

 

}
