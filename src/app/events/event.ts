import { Venue } from './venue';
export class Event {
  id: string;
  name: string;
  date: string;
  time: string;
  venue: Venue;

}
