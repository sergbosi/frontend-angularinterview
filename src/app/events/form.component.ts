import { Component, OnInit } from '@angular/core';
import { Venue } from './venue';
import {Event} from './event'
import {EventService} from './event.service'
import {Router, ActivatedRoute} from '@angular/router'
import swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})

export class FormComponent implements OnInit {

  private Event: Event = new Event()
  private Venue: Venue = new Venue()
 

  constructor(private EventService: EventService,
  private router: Router,
  private activatedRoute: ActivatedRoute) { }

ngOnInit() {
    this.Event.venue = new Venue()
  }

 /* ngOnInit() {
    this.cargarEvent()
  }

  cargarEvent(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.EventService.getEvent(id).subscribe( (Event) => this.Event = Event)
      }
    })
  }*/

  create(): void {
  console.log(this.Event)
  console.log(this.Venue)
  this.Event.venue = this.Venue
  console.log(this.Event)
  
    this.EventService.create(this.Event)
      .subscribe(Event => {
        this.router.navigate(['/Events'])
        swal('New Event', `Event ${Event.name} created`, 'success')
      }
      );
  }

 
  
}
