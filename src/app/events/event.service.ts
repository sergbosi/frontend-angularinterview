import { Injectable } from '@angular/core';
 
import { Venue } from './venue';
import { Event } from './event';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
 


@Injectable()
export class EventService {
  private urlEndPoint: string = 'http://localhost:8080/api/events';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

 

  constructor(private http: HttpClient) { }
 


  getEvents(): Observable<Event[]> {
    //return of(EventS);
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Event[])
    );
  }

  create(Event: Event) : Observable<Event> {

    return this.http.post<Event>(this.urlEndPoint+'/form', Event, {headers: this.httpHeaders})
  }


 

}
